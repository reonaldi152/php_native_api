<?php
// Membuat API Product

include "function.php";

$query = mysqli_query($db, "SELECT * FROM users");
$jsonArray = array();

// $photo = "https://localhost/img";

while ($product = mysqli_fetch_assoc($query)) {

    $rows['id'] = $product['id'];
    $rows['nama'] = $product['nama'];
    $rows['email'] = $product['email'];
    $rows['password'] = $product['password'];
    $rows['address'] = $product['address'];
    $rows['no_rumah'] = $product['no_rumah'];
    $rows['no_telp'] = $product['no_telp'];
    $rows['kota'] = $product['kota'];

    array_push($jsonArray, $rows);
}

echo json_encode($jsonArray, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
