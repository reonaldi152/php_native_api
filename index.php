<?php

session_start();

// jika tidak ada session login, kembalikan ke halaman login
if (!isset($_SESSION["login"])) {
    header("Location: login.php");
}

require "function.php";

$produk = query("SELECT * FROM produk");

if (isset($_POST["cari"])) {
    $result = cari($_POST["keyword"]);
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk</title>
</head>

<body>

    <h1>Daftar Produk</h1>

    <br>
    <a href="logout.php" style="width: 50px; background-color: red;">Logout</a>

    <br><br>

    <a href="tambah.php">tambah data produk</a>

    <br><br>

    <form action="" method="post">

        <input type="text" name="keyword" size="40" autofocus placeholder="masukkan keyword pencarian" autocomplete="off">
        <!-- autofocus untuk otomatis aktif di aplikasi -->
        <!-- autocomplete agar history nya kehapus, atau dimatikan -->
        <button type="submit" name="cari">Cari</button>
    </form>
    <br><br>

    <table border="1" cellpadding="10" cellspacing="0">

        <tr>
            <th>No.</th>
            <th>NAMA PRODUK</th>
            <th>DESKRIPSI PRODUK</th>
            <th>HARGA PRODUK</th>
            <th>GAMBAR PRODUK</th>
            <th>RATING PRODUK</th>
            <th>TIPE PRODUK</th>
            <th>Aksi</th>
        </tr>

        <?php $i = 1; ?>
        <?php foreach ($produk as $row) : ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $row["nama"]; ?></td>
                <td><?php echo $row["deskripsi"]; ?></td>
                <td><?php echo $row["harga"]; ?></td>
                <td><img src="img/<?php echo $row["gambar"]; ?>" alt="gambar" width="50"></td>
                <td><?php echo $row["rating"]; ?></td>
                <td><?php echo $row["tipe"]; ?></td>
                <td>
                    <a href="update.php?id=<?php echo $row["id"]; ?>">Edit</a> |
                    <a href="hapus.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('yakin?')">Delete</a>
                </td>
            </tr>
            <?php $i++; ?>
        <?php endforeach; ?>

    </table>

</body>

</html>