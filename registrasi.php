<?php

include "function.php";

if (isset($_POST["register"])) {

    if (registrasi($_POST) > 0) {
        echo "<script>
            alert('user baru berhasil ditambahkan');
            document.location.href = 'login.php';
        </script>";
    } else {
        echo mysqli_error($db);
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>

    <style>
        label {
            display: block;
        }
    </style>
</head>

<body>

    <h1>Halaman Registrasi</h1>

    <form action="" method="post">

        <ul>
            <li>
                <label for="nama">Nama : </label>
                <input type="text" name="nama" id="nama">
            </li>

            <li>
                <label for="email">Email : </label>
                <input type="text" name="email" id="email">
            </li>

            <li>
                <label for="password">Password : </label>
                <input type="password" name="password" id="password">
            </li>

            <li>
                <label for="password2">Konfirmasi Password : </label>
                <input type="password" name="password2" id="password2">
            </li>

            <li>
                <label for="address">address : </label>
                <textarea name="address" id="address" cols="30" rows="10"></textarea>
            </li>

            <li>
                <label for="no_rumah">Nomor Rumah : </label>
                <input type="text" name="no_rumah" id="no_rumah">
            </li>

            <li>
                <label for="no_telp">No Telepon : </label>
                <input type="text" name="no_telp" id="no_telp">
            </li>

            <li>
                <label for="kota">Asal Kota : </label>
                <input type="text" name="kota" id="kota">
            </li>

            <li>
                <label for="status">Status : </label>
                <input type="text" name="status" id="status">
            </li>

            <li>
                <button type="submit" name="register">Register</button>
            </li>
        </ul>
    </form>

</body>

</html>