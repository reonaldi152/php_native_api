<?php
include "function.php";


// header hasil berbentuk json
header("Content-Type:application/json");

$method = $_SERVER['REQUEST_METHOD'];

$limit = isset($_GET['limit']) ? (int) $_GET['limit'] : 1;

$sql_limit = '';
if (!empty($limit)) {
    $sql_limit = ' LIMIT 0,' . $limit;
}

$list = mysqli_query($db, 'SELECT * FROM transaksi ' . ' ' . $sql_limit);

$result = array();

$user_id = $_GET['user_id'];
$produk_id = $_GET['produk_id'];
$kuantitas = $_GET['kuantitas'];
$total = $_GET['total'];
// status ada PENDING, ON_DELIVERY, DELIVERED, CANCELLED
$status = $_GET['status'];
$payment_url = $_GET['payment_url'];


mysqli_query($db, "INSERT INTO transaksi VALUES('','$user_id','$produk_id','$kuantitas','$total','$status','$payment_url')");

$query = mysqli_query($db, "SELECT * FROM transaksi");

if ($method == 'POST') {

    $result['meta'] = [
        "code" => 200,
        "status" => 'success',
        "message" => "Transaksi Berhasil"
    ];

    $result['data'] = [
        "data" => $query->fetch_all(MYSQLI_ASSOC)
    ];
} else {
    $result['status'] = [
        "code" => 400,
        "status" => 'failed'
    ];
}

echo json_encode($result);
