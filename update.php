<?php
include "function.php";

$id = $_GET["id"];

$prdk  = query("SELECT * FROM produk WHERE id = $id")[0];


if (isset($_POST["submit"])) {

    if (update($_POST) > 0) {

        echo "
            <script>
                alert('data berhasil diubah');
                document.location.href = 'index.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('data gagal diubah');
            
            </script>
        " . PHP_EOL;
        echo mysqli_error($db);
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data Produk</title>
</head>

<body>

    <h1>Update Data Produk</h1>

    <form action="" method="post" enctype="multipart/form-data">
        <ul>
            <input type="hidden" name="id" value="<?php echo $prdk["id"] ?>">
            <input type="hidden" name="gambarLama" value="<?php echo $mhs["gambar"] ?>">
            <!--  jadi gambar lama dikirimkan, jika user ganti gambar, gambar baru yg dikirimkan -->
            <li>
                <label for="nama">nama : </label>
                <input type="text" name="nama" id="nama" required value="<?php echo $prdk["nama"] ?>">
                <!-- for kaitannya dengann id, jika label di klik maka akan ke klik juga yg input -->
            </li>

            <li>
                <label for="deskripsi">deskripsi : </label>
                <input type="text" name="deskripsi" id="deskripsi" required value="<?php echo $prdk["deskripsi"] ?>">
            </li>

            <li>
                <label for="harga">harga : </label>
                <input type="text" name="harga" id="harga" required value="<?php echo $prdk["harga"] ?>">
            </li>

            <li>
                <label for="gambar">Gambar : </label>
                <img src="img/<?php echo $prdk["gambar"] ?>" alt="" width="40px">
                <br>
                <input type="file" name="gambar" id="gambar">
            </li>

            <li>
                <label for="gambar">Gambar : </label>
                <input type="text" name="gambar" id="gambar" required value="<?php echo $prdk["gambar"] ?>">
            </li>

            <li>
                <label for="rating">rating : </label>
                <input type="text" name="rating" id="rating" required value="<?php echo $prdk["rating"] ?>">
            </li>

            <li>
                <label for="tipe">tipe : </label>
                <input type="text" name="tipe" id="tipe" required value="<?php echo $prdk["tipe"] ?>">
            </li>

            <li>
                <button type="submit" name="submit">Update Data</button>
            </li>
        </ul>
    </form>



</body>

</html>