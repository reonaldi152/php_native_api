<?php

include "function.php";


// header hasil berbentuk json
header("Content-Type:application/json");

$method = $_SERVER['REQUEST_METHOD'];

$query = mysqli_query($db, "SELECT * FROM users");
$userToken = mysqli_query($db, "SELECT access_token FROM users");


$result = array();

if ($method == 'POST') {

    $result['meta'] = [
        "code" => 200,
        "status" => 'success'
    ];

    $result['data'] = [
        "token_type" => 'Bearer',
        "user" => $query->fetch_all(MYSQLI_ASSOC)
    ];
} else {
    $result['status'] = [
        "code" => 400,
        "status" => 'failed'
    ];
}

echo json_encode($result);
