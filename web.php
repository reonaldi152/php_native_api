<?php
// function getAuthorizationHeader()
// {
//     $headers = null;
//     if (isset($_SERVER['Authorization'])) {
//         $headers = trim($_SERVER["Authorization"]);
//     } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
//         $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
//     } elseif (function_exists('apache_request_headers')) {
//         $requestHeaders = apache_request_headers();
//         // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
//         $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
//         //print_r($requestHeaders);
//         if (isset($requestHeaders['Authorization'])) {
//             $headers = trim($requestHeaders['Authorization']);
//         }
//     }
//     return $headers;
// }

// function getBearerToken()
// {
//     $headers = getAuthorizationHeader();
//     // HEADER: Get the access token from the header
//     if (!empty($headers)) {
//         if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
//             return $matches[1];
//         }
//     }
//     return null;
// }

// var_dump(getAuthorizationHeader());
// var_dump(getBearerToken());


$url = "http://localhost/tahupalingtahu/userapi.php";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
    "Accept: application/json",
    "Authorization: Bearer {token}",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);
var_dump($resp);
